# TurnipCrossing
Friend turnip trade network

# Contributing to TurnipCrossing
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Develop with Gitlab
We use gitlab to host code, to track issues and feature requests, as well as accept forked merge requests

## We Use [Github Flow](https://guides.github.com/introduction/flow/), So All Code Changes Happen Through Merge Requests
Merge requests are the best way to propose changes to the codebase (we use [Github Flow](https://guides.github.com/introduction/flow/)). We actively welcome your merge requests:

1. Fork the repo and create your branch from `dev`.
2. If you've added code that should be tested, add tests.
3. If you've changed something, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that merge request!

## Any contributions you make will be under the MIT Software License
In short, when you submit code changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report bugs using Gitlab's [issues](https://gitlab.com/romerramos/turnip_crossing/-/issues)
We use GitHub issues to track public bugs. Report a bug by [opening a new issue](); it's that easy!

## Write bug reports with detail, background, and sample code
[This is an example](http://stackoverflow.com/q/12488905/180626) of a bug report, and I think it's not a bad model. Here's [another example from Craig Hockenberry](http://www.openradar.me/11905408).

**Great Bug Reports** tend to have:

- A quick summary and/or background
- Steps to reproduce
  - Be specific!
  - Give sample code if you can.
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)

People *love* thorough bug reports. I'm not even kidding.

## Use a Consistent Coding Style

* Modified version of Airbnb styles for [Ruby](https://github.com/airbnb/ruby) and [Javascript](https://github.com/airbnb/javascript)
* 2 spaces for indentation rather than tabs
* Don't use incomplete names for your variables, methods, etc e.g:
  - document_category -> OK :)
  - doc_category -> NOT OK!
  - document_cat -> NOT OK! (cats!?)
  - d_c -> PLEASE NO!

## License
By contributing, you agree that your contributions will be licensed under its MIT License.

## References
This document was adapted from [this template](https://gist.github.com/briandk/3d2e8b3ec8daf5a27a62)