# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TurnipCrossing
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified
    # here. Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.generators do |g|
      g.fixture_replacement :factory_bot, dir: 'spec/factories'
      g.controller_specs false
    end
    # Configuracion de environment variables.yml
    config.before_configuration do
      env_file = Rails.root.join('config/environment_variables.yml').to_s
      branch_file = Rails.root.join('tmp/rev_branch').to_s
      user_file = Rails.root.join('tmp/rev_user').to_s

      # Variables de configuracion de entornos
      if File.exist?(env_file)
        YAML.load_file(env_file)[Rails.env].each do |key, value|
          ENV[key.to_s] = value
        end
      end

      # Variables de configuracion de Ribbon - Rama Review
      ENV['REVIEW_BRANCH'] = if File.exist?(branch_file)
                               File.read(branch_file)
                             else
                               'N/A'
                             end

      # Variables de configuracion de Ribbon - Usuario del deploy
      ENV['REVIEW_USER'] = if File.exist?(user_file)
                             File.read(user_file)
                           else
                             'N/A'
                           end
    end
  end
end
