# frozen_string_literal: true

# config valid for current version and patch releases of Capistrano
lock '~> 3.12.1'

env_file = './config/environment_variables.yml'
if File.exist?(env_file)
  YAML.load_file(env_file)['capistrano'].each do |key, value|
    ENV[key.to_s] = value
  end
end

set :application, ENV['PROJECT_NAME']
set :repo_url, ENV['REPO_URL']

# Deploy to the user's home directory
set :deploy_to, "/home/deploy/#{fetch :application}"

set :linked_files, %w[config/environment_variables.yml]
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache',
       'tmp/sockets', 'vendor/bundle', '.bundle', 'public/system',
       'public/uploads'

# Deployment branch Variables
set :commit, ENV['CI_COMMIT_SHA'] ? ENV['CI_COMMIT_SHA'][0..7] : 'HEAD'

if ENV['GITLAB_USER_EMAIL']
  set :user, ENV['GITLAB_USER_EMAIL'].split('@').first
else
  set :user, 'local user'
end

set :time, Time.now.localtime('-04:00').strftime('%d/%m %I:%M%p')

# Only keep the last 5 releases to save disk space
set :keep_releases, 5

# before "deploy:assets:precompile", "deploy:yarn_install"

namespace :deploy do
  # desc 'Run rake yarn:install'
  # task :yarn_install do
  #   on roles(:web) do
  #     within release_path do
  #       execute("cd #{release_path} && yarn install")
  #     end
  #   end
  # end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc 'Reset environment (rake db:reset)'
  task :db_reset do
    on roles(:app) do
      within current_path.to_s do
        with rails_env: fetch(:rails_env).to_s do
          execute :rake, 'db:migrate:reset db:seed'
        end
      end
    end
  end

  desc 'Set deployed branch'
  task :set_deployed_branch do
    on roles(:app) do
      within current_path.to_s do
        branchfile = release_path.join('tmp/rev_branch')
        userfile = release_path.join('tmp/rev_user')

        execute "echo '#{fetch(:branch)}@#{fetch(:commit)}' >> #{branchfile}"
        execute "echo '#{fetch(:user)} el #{fetch(:time)}' >> #{userfile}"
      end
    end
  end

  after :publishing, 'deploy:set_deployed_branch'
  after :published, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end

# Optionally, you can symlink your database.yml and/or secrets.yml file from
# the shared directory during deploy
# This is useful if you don't want to use ENV variables
# append :linked_files, 'config/secrets.yml'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options,
#   command_output: true,
#   log_file: "log/capistrano.log",
#   color: :auto,
#   truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
# append :linked_dirs,
#   "log",
#   "tmp/pids",
#   "tmp/cache",
#   "tmp/sockets",
#   "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key
# before first deploy.
# set :ssh_options, verify_host_key: :secure
