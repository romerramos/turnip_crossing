const { environment } = require('@rails/webpacker')

environment.loaders.append('import-glob', {
  test: /\.scss$/,
  use: 'import-glob-loader',
})

module.exports = environment
