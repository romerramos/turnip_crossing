process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const environment = require('./environment')

const esLinterLoader = {
  enforce: 'pre',
  test: /\.js$/,
  exclude: /node_modules/,
  loader: 'eslint-loader'
}

environment.loaders.prepend('eslint', esLinterLoader)

module.exports = environment.toWebpackConfig()
