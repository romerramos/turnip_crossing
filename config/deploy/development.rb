# frozen_string_literal: true

set :stage, :development
set :rails_env, :development_remote

set :bundle_without, 'empty'
set :bundle_flags, '--deployment'

# Server definition: (define ip in local env, and pass in gitlab)
# server ENV['DEV_DROPLET_IP'], user: 'deploy', roles: %w{web app db},
#   password: ENV['SSH_DEPLOY_PASS']
server ENV['DEV_DROPLET_IP'], user: 'deploy', roles: %w[web app db]

# Set :branch var from CI environment
set :branch, ENV['CI_COMMIT_REF_NAME'] || 'dev'
# Set :branch var from the CLI
set :branch, ENV['DEPLOY_BRANCH'] if ENV['DEPLOY_BRANCH']

# Capistrano Deployment Route
set :deploy_to, "/home/deploy/dev.#{ENV['PROJECT_NAME']}"

namespace :deploy do
  before :finishing, 'deploy:db_reset'
end
