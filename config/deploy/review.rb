# frozen_string_literal: true

set :rails_env, :review
set :application, ENV['SUBDOMAIN']
set :deploy_to, "/home/deploy/reviews/#{fetch :application}"
server ENV['REVIEW_DROPLET_IP'], user: 'deploy', roles: %w[web app db]
set :branch, ENV['BRANCH']
set :linked_files, %w[db/review.sqlite3]

namespace :deploy do
  task :undeploy do
    on roles(:app) do
      invoke 'bundle exec passenger stop'
      execute "rm -rf #{fetch(:deploy_to)}"
    end
  end
  namespace :check do
    # The database needs to exist before it's symlinked in or the symlink
    # will fail.
    before :linked_files, :create_db do
      on primary :db do
        execute "touch #{shared_path}/db/review.sqlite3"
      end
    end
  end
end
