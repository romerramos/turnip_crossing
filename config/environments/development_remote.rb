# frozen_string_literal: true

require Rails.root.join('config/environments/development_shared')

# Configuraciones especificas del entorno development (remoto)
Rails.application.configure do
  # Para uso con mailers (opcional, activar en shared y aca)
  options = { host: 'dev.conciliar.com.ve' }
  config.action_mailer.default_url_options   = options
  config.action_mailer.asset_host            = "https://#{options[:host]}"
  config.action_controller.asset_host        = options[:host]
  config.action_mailer.raise_delivery_errors = true
end
