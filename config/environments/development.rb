# frozen_string_literal: true

require Rails.root.join('config/environments/development_shared')

# Configuraciones especificas del entorno local
Rails.application.configure do
  # Verifies that versions and hashed value of the package contents in the
  # project's package.json
  # Para uso con mailers (opcional, activar en shared y aca)
  config.webpacker.check_yarn_integrity = false

  options = { host: 'localhost', port: '3000' }
  config.action_mailer.default_url_options = options
  config.action_mailer.asset_host = "http://#{options[:host]}:#{options[:port]}"
  # Comentado para permitir conexiones en LAN al servidor
  # config.action_controller.asset_host = "#{options[:host]}:#{options[:port]}"
  config.action_mailer.raise_delivery_errors = true
end
