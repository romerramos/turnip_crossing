# frozen_string_literal: true

# Helpers to handle JSON requests
module RequestSpecHelper
  def json
    # JSON.parse(response.body)
    response.parsed_body
  end
end
