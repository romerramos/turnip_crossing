# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  config.before(:each, type: :system, js: true) do
    # driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
    driven_by :selenium_chrome_headless,
              using: :chrome,
              screen_size: [1400, 1400]
  end
end
