# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |number| "person_#{number}@example.com" }
    password { 'password' }
    password_confirmation { 'password' }
  end
end
