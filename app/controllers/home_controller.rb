# frozen_string_literal: true

class HomeController < ApplicationController
  layout 'simple'
  def index; end
end
