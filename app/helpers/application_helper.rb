# frozen_string_literal: true

module ApplicationHelper
  def flash_type
    return 'notice' if notice.present?

    'alert' if alert.present?
  end

  def flash_message_text
    send(flash_type) if flash_type
  end
end
