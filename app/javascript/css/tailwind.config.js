module.exports = {
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#58BCA8',
          200: '#4cbca6',
          300: '#419989',
          500: '#005961',
          700: '#00373F'
        },
        secondary: '#58BCA8',
        pastel: {
          100: '#f8f5e6',
          300: '#f9db9d',
          350: '#C0B595',
          700: '#54534e'
        },
        danger: '#d54b65',
        info: '#308bda',
        yellow: '#f8e076'
      },
      fontFamily: {
        basic: ['Comic Neue']
      }
    }
  },
  variants: {},
  plugins: []
}
